#include <SDL.h>
#include <SDL_image.h>
#include <math.h>


const int iMap[10][10] = {
	1,1,1,1,1,1,1,1,1,1,
	1,0,0,0,0,0,0,0,0,1,
	1,0,1,1,0,1,0,1,0,1,
	1,0,0,1,1,0,1,0,0,1,
	1,0,1,0,0,0,0,1,0,1,
	1,0,0,1,0,0,1,0,0,1,
	1,0,1,0,0,0,0,1,0,1,
	1,0,0,0,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,1,
	1,1,1,1,1,1,1,1,1,1,
}; 


float Distance(float x, float x0, float y, float y0) {
	return sqrt(pow(x - x0, 2) + pow(y - y0, 2));
}


int main(int argc, char* argv[]) {

#pragma region  Variables
	SDL_Rect rect = { 0,0,0,0 };
	int xCord = 400;
	int yCord = 300; //spawn point - middle of the map
	int gr = 0, bl = 0;
	float movX = 0;
	float movY = 0;
	float pAngle = 0;
	float fFov = 0.7850;
	float rAngle = 0;
	float curDistance = 0;
	float maxDistance = 400;
	movX = sin(pAngle) * 3;
	movY = cos(pAngle) * 3;
#pragma endregion



	if (SDL_Init(SDL_INIT_VIDEO) == 0) {
		SDL_Window* window = NULL;
		SDL_Renderer* renderer = NULL;

		if (SDL_CreateWindowAndRenderer(800, 600, 0, &window, &renderer) == 0) {

			SDL_bool done = SDL_FALSE;

			while (!done) {

				SDL_Event event;

				while (SDL_PollEvent(&event)) {
					
					switch (event.type) {
					case SDL_QUIT:
						done = SDL_TRUE;
						break;
					case SDL_KEYDOWN:
						switch (event.key.keysym.sym) {
						case SDLK_LEFT:
							pAngle -= 0.08;
							if (pAngle < 0)
								pAngle = 3.14 * 2;
							movX = sin(pAngle);
							movY = cos(pAngle);
							break;
						case SDLK_RIGHT:
							pAngle += 0.08;
							if (pAngle > 3.14 * 2)
								pAngle = 0;
							movX = sin(pAngle);
							movY = cos(pAngle);
							break;
						case SDLK_UP:
							xCord += movX * 5;
							yCord += movY * 5;
							break;
						case SDLK_DOWN:
							xCord -= movX * 5;
							yCord -= movY * 5;
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
				}

				SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
				SDL_RenderClear(renderer);

				int xAxis = 0;
				rAngle = pAngle - (fFov / 2);

				while (rAngle < pAngle + (fFov / 2)) {


					for (int x = 0; x < maxDistance; x++) {

						int xPos = xCord + x * sin(rAngle);
						int yPos = yCord + x * cos(rAngle);

						if (iMap[yPos / 60][xPos / 80]) {

							int objDist = Distance(xCord, xPos, yCord, yPos);//accurate distance
							int dist = objDist * cos(pAngle - rAngle);//fisheye fix

							for (int yAxis = dist; yAxis < 600 - dist; yAxis++) {

								gr -= objDist * 1.5;
								bl -= objDist * 1.5;
								if (gr < 0) {
									gr = 0;
									bl = 0;
								}

								SDL_SetRenderDrawColor(renderer, 0, gr, bl, 255);
								rect.x = xAxis;
								rect.y = yAxis;
								rect.h = 1;
								rect.w = 10;
								SDL_RenderFillRect(renderer, &rect);

								gr = 200;
								bl = 200;
							}
							break;
						}

					}
					xAxis += 10;
					rAngle += 0.01;
				}
				SDL_RenderPresent(renderer);

			}
		}
	}

	return 0;
}